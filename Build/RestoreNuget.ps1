﻿$RootDir = (get-item $PSScriptRoot).Parent.FullName

# Nuget all the packages for the main solution
"Attempting restore of nuget packages..."
$nuget = Join-Path $RootDir ".nuget\nuget.exe"

$solution = Join-Path $RootDir VMob.LoadTesting.sln
$command = $nuget + " restore " + $solution

"About to invoke command " + $command

Invoke-Expression -Command:$command

exit $LASTEXITCODE

