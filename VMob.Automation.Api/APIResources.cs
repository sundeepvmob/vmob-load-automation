﻿namespace VMob.Automation.Api
{
    public static class ApiResources
    {
        //Azme Resources

        #region Fields

        public static string CreateAnonymousRegistrationResource = "v3/anonymousRegistrations";
        public static string CreateCrossreferenceResource = "v3/crossReferences";
        public static string CreateEmailRegistrationResource = "v3/emailRegistrations";
        public static string CreateFavoriteContentResource = "v3/consumers/favouriteContent";
        public static string CreateLocationCheckinResource = "v3/Activities";
        public static string CreateLoyaltyCardPointsResource = "v3/consumers/points";
        public static string CreateOfferClickResource = "v3/Activities";
        public static string CreateOfferImpressionResource = "v3/Activities";
        public static string CreateRedeemedOffersResource = "v3/consumers/redeemedOffers";
        public static string GetAdvertisementsResource = "v3/advertisements?channel={0}&placement={1}&limit=100&offset=0";
        public static string GetConfigurations = "v3/configurations";
        public static string GetGeofences = "v3/geofences?longitude={0}&latitude={1}";
        public static string GetWeightedContentResource = "v3/consumers/weightedContent?latitude={0}&longitude={1}&offset=0&limit=100";
        public static string ListLoyaltyCards = "v3/loyaltyCards";
        public static string ListRedeemedOffersResource = "v3/consumers/redeemedOffers";
        public static string SearchOffersResource = "v3/offers?";

        #endregion
    }
}