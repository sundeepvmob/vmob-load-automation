﻿using RestSharp;
using System;
using VMob.Automation.Api.Tool;
using VMob.SDK.Model.Activities;

namespace VMob.Automation.Api.EndPoints
{
    public class ActivityEndpoint : BaseEndpoint
    {
        #region Properties

        public override string BaseUrl
        {
            get { return "https://act-VMob-Load.vmobapps.com"; }
        }

        #endregion

        #region Methods

        public void CreateLocationCheckin(string accessToken)
        {
            Console.WriteLine(@"In CreateLocationCheckin");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateLocationCheckinResource);
            ActivityModel data = new ActivityModel();
            data.id = Guid.NewGuid().ToString();
            data.sourceActivityTime = "2014-03-14T02:14:00Z";
            data.sourceActivityTimeZoneOffset = "+13:00";
            data.actionTypeCode = "1";
            data.actionValue1 = "gps";
            data.actionValue2 = "15";
            data.actionValue3 = "30.1231231;0.34;87";
            data.latitude = (decimal)-36.843;
            data.longitude = (decimal)174.755;
            request.AddBody(data);

            var response = ExecuteJsonRequest<object>(request);
            StatusCode = response.StatusCode;
        }

        public void CreateOfferClick(string accessToken)
        {
            Console.WriteLine(@"In CreateOfferClick");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateOfferClickResource);
            offerImpression data = new offerImpression();
            data.id = Guid.NewGuid().ToString();
            data.sourceActivityTime = "2014-03-14T02:14:00Z";
            data.sourceActivityTimeZoneOffset = "+13:00";
            data.actionTypeCode = "5";
            data.actionCode = "Premium";
            data.itemId = 3156;
            data.itemCode = "O";
            request.AddBody(data);
            var response = ExecuteJsonRequest<object>(request);
            StatusCode = response.StatusCode;
        }

        public void CreateOfferImpression(string accessToken)
        {
            Console.WriteLine(@"In CreateOfferImpression");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateOfferImpressionResource);
            offerImpression data = new offerImpression();
            data.id = Guid.NewGuid().ToString();
            data.sourceActivityTime = "2014-03-14T02:14:00Z";
            data.sourceActivityTimeZoneOffset = "+13:00";
            data.actionTypeCode = "3";
            data.actionCode = "Premium";
            data.itemId = 648;
            data.itemCode = "O";
            request.AddBody(data);
            var response = ExecuteJsonRequest<object>(request);
            StatusCode = response.StatusCode;
        }

        #endregion
    }

    internal class ActivityModel : activity
    {
    }
}