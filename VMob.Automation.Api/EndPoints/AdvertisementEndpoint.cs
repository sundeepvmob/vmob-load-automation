﻿using RestSharp;
using System;
using VMob.Automation.Api.Tool;
using VMob.SDK.Model.Advertisements;

namespace VMob.Automation.Api.EndPoints
{
    public class AdvertisementEndpoint : BaseEndpoint
    {
        #region Properties

        public override string BaseUrl
        {
            get { return "https://adv-VMob-Load.vmobapps.com"; }
        }

        #endregion

        #region Methods

        public void GetAdvertisements(string accessToken)
        {
            Console.WriteLine(@"In GetAdvertisements");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, String.Format(ApiResources.GetAdvertisementsResource, "MAPP", "TB"));
            var response = ExecuteJsonRequest<advertisement>(request);
            StatusCode = response.StatusCode;
        }

        #endregion
    }
}