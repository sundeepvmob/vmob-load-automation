﻿using RestSharp;
using System;
using System.Net;
using VMob.Automation.Api.Tool;

namespace VMob.Automation.Api.EndPoints
{
    public class BaseEndpoint
    {
        #region Fields

        public string Token;

        #endregion

        #region Properties

        public virtual string BaseUrl
        {
            get { return "https://con-VMob-Load.vmobapps.com"; }
        }

        public HttpStatusCode StatusCode { get; set; }

        #endregion

        #region Methods

        public virtual void AddHeaderForJsonRequest(JsonRestRequest request)
        {
            Guid guid = Guid.NewGuid();
            request.AddHeader("x-vmob-uid", guid.ToString());
            request.AddHeader("x-vmob-authorization", "e681e939-8424-4832-9931-42db645203c1");
            request.AddHeader("x-vmob-device", "Google Nexus");
            request.AddHeader("x-vmob-device_type", "a");
            request.AddHeader("x-vmob-device_network_type", "3G");
            request.AddHeader("x-vmob-device_screen_resolution", "1080x940");
            request.AddHeader("x-vmob-mobile_operator", "Vodafone");
            request.AddHeader("x-vmob-location_latitude", "-28.12513");
            request.AddHeader("x-vmob-location_longitude", "168.12156");
            request.AddHeader("x-vmob-location_accuracy", "4.5");
            request.AddHeader("x-vmob-device_utc_offset", "+12:00");
            request.AddHeader("Accept-Language", "en-NZ");
            request.AddHeader("x-vmob-application_version", "1.0.0.123");
            request.AddHeader("x-vmob-device_os_version", "2.005.3.5");
            request.AddHeader("User-Agent", "...");
            if (!String.IsNullOrEmpty(Token)) request.AddHeader("Authorization", string.Format("Bearer {0}", Token));
        }

        public IRestResponse<T> ExecuteJsonRequest<T>(JsonRestRequest request) where T : new()
        {
            RestClient client = new RestClient(BaseUrl);
            client.AddHandler("application/json", new JsonDeserializer());
            AddHeaderForJsonRequest(request);
            IRestResponse<T> response = client.Execute<T>(request);
            return response;
        }

        #endregion
    }
}