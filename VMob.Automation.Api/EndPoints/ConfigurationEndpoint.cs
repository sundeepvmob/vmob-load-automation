﻿using RestSharp;
using System;
using VMob.Automation.Api.Tool;
using VMob.SDK.Model.Configuration;

namespace VMob.Automation.Api.EndPoints
{
    public class ConfigurationEndpoint : BaseEndpoint
    {
        #region Properties

        public override string BaseUrl
        {
            get { return "https://cfg-VMob-Load.vmobapps.com"; }
        }

        #endregion

        #region Methods

        public void GetConfigurations(string accessToken)
        {
            Console.WriteLine(@"In GetConfigurations");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, ApiResources.GetConfigurations);
            var response = ExecuteJsonRequest<configuration>(request);
            StatusCode = response.StatusCode;
        }

        #endregion
    }
}