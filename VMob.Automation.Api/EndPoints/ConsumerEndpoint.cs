﻿using RestSharp;
using System;
using System.Collections.Generic;
using VMob.Automation.Api.Tool;
using VMob.SDK.Model.Consumers;

namespace VMob.Automation.Api.EndPoints
{
    public class ConsumerEndpoint : BaseEndpoint
    {
        #region Properties

        public override string BaseUrl
        {
            get { return "https://con-VMob-Load.vmobapps.com"; }
        }

        #endregion

        #region Methods

        public void CreateCrossreference(string accessToken)
        {
            Console.WriteLine(@"In CreateCrossreference");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateCrossreferenceResource);
            crossReference data = new crossReference();
            data.externalId = string.Concat(Guid.NewGuid().ToString().Replace("-", ""), Guid.NewGuid().ToString().Replace("-", ""));
            data.systemType = "0";
            request.AddBody(data);
            var response = ExecuteJsonRequest<object>(request);
            StatusCode = response.StatusCode;
        }

        public void CreateFavoriteContent(string accessToken)
        {
            Console.WriteLine(@"In CreateFavoriteContent");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateFavoriteContentResource);
            favouriteContentRequest data = new favouriteContentRequest();
            data.contentId = "1";
            data.contentType = "offer";
            request.AddBody(data);
            var response = ExecuteJsonRequest<object>(request);
            StatusCode = response.StatusCode;
        }

        public LoyaltyPointsResponse CreateLoyaltyCardPoints(string accessToken)
        {
            Console.WriteLine(@"In CreateLoyaltyCardPoints");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateLoyaltyCardPointsResource);
            points data = new points();
            data.loyaltyCardId = 1;
            data.pointsRequested = 3;
            data.transactionId = Guid.NewGuid().ToString();
            data.autoActivateReward = true;
            data.fillMultipleCards = false;
            request.AddBody(data);
            var response = ExecuteJsonRequest<LoyaltyPointsResponse>(request);
            StatusCode = response.StatusCode;
            return response.Data;
        }

        public void CreateRedeemedOffer(string accessToken)
        {
            Console.WriteLine(@"In CreateRedeemedOffer");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateRedeemedOffersResource);
            redeemedOffer data = new redeemedOffer();
            data.offerId = 1;
            data.giftId = null;
            data.offerInstanceUniqueId = null;
            request.AddBody(data);
            var response = ExecuteJsonRequest<redeemedOffer>(request);
            StatusCode = response.StatusCode;
        }

        public void GetWeightedContent(string accessToken)
        {
            Console.WriteLine(@"In GetWeightedContent");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, String.Format(ApiResources.GetWeightedContentResource, "-28.12513", "168.12156"));
            var response = ExecuteJsonRequest<weightedContentEvent>(request);
            StatusCode = response.StatusCode;
        }

        public void ListRedeemedOffers(string accessToken)
        {
            Console.WriteLine(@"In ListRedeemedOffers");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, ApiResources.ListRedeemedOffersResource);
            var response = ExecuteJsonRequest<redeemedOffer>(request);
            StatusCode = response.StatusCode;
        }

        public void RedeemedRewardOffer(string accessToken, string offerInstanceUniqueId)
        {
            Console.WriteLine(@"In RedeemedRewardOffer");
            Token = accessToken;
            var request = new JsonRestRequest(Method.POST, ApiResources.CreateRedeemedOffersResource);
            redeemedOffer data = new redeemedOffer();
            data.offerId = 89;
            data.giftId = null;
            data.offerInstanceUniqueId = offerInstanceUniqueId;
            request.AddBody(data);
            var response = ExecuteJsonRequest<redeemedOffer>(request);
            StatusCode = response.StatusCode;
        }

        #endregion

        #region Classes

        public class LoyaltyPointsResponse : pointsResponse
        {
            #region Properties

            public List<Summary> PointCreationSummary { get; set; } = new List<Summary>();

            #endregion
        }

        public class Summary
        {
            #region Properties

            public string InstanceId { get; set; }
            public string OfferInstanceUniqueId { get; set; }
            public int OutcomeCode { get; set; }
            public string OutcomeDescription { get; set; }
            public int PointsAllocated { get; set; }
            public int PointsBalance { get; set; }

            #endregion
        }

        #endregion
    }
}