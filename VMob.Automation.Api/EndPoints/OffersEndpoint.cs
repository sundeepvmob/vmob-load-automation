﻿using RestSharp;
using System;
using VMob.Automation.Api.Tool;
using VMob.SDK.Model.Messaging;
using VMob.SDK.Model.Offers;

namespace VMob.Automation.Api.EndPoints
{
    public class OffersEndpoint : BaseEndpoint
    {
        #region Properties

        public override string BaseUrl
        {
            get { return "https://off-VMob-Load.vmobapps.com"; }
        }

        #endregion

        #region Methods

        public void GetGeofences(string accessToken)
        {
            Console.WriteLine(@"In GetGeofences");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, String.Format(ApiResources.GetGeofences, "168.12156", "-28.12513"));
            var response = ExecuteJsonRequest<geoFence>(request);
            StatusCode = response.StatusCode;
        }

        public void ListLoyaltyCards(string accessToken)
        {
            Console.WriteLine(@"In GetGeofences");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, String.Format(ApiResources.ListLoyaltyCards));
            var response = ExecuteJsonRequest<loyaltyCard>(request);
            StatusCode = response.StatusCode;
        }

        public void SearchOffers(string accessToken)
        {
            Console.WriteLine(@"In SearchOffers");
            Token = accessToken;
            var request = new JsonRestRequest(Method.GET, String.Format(ApiResources.SearchOffersResource));
            var response = ExecuteJsonRequest<offer>(request);
            StatusCode = response.StatusCode;
        }

        #endregion
    }
}