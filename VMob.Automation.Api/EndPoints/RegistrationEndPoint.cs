﻿using RestSharp;
using System;
using VMob.Automation.Api.Tool;
using VMob.SDK.Model.Consumers;
using VMob.SDK.Model.Consumers.IO;

namespace VMob.Automation.Api.EndPoints
{
    public class RegistrationEndPoint : BaseEndpoint
    {
        #region Methods

        public accessTokenResponse CreateAnonymousRegistration()
        {
            Console.WriteLine(@"In CreateAnonymousRegistration");
            anonymousRegistrationRequest registration = new anonymousRegistrationRequest();
            registration.grant_type = "password";
            registration.username = Guid.NewGuid().ToString();
            registration.password = String.Concat(Guid.NewGuid().ToString(), "_password");

            var request = new JsonRestRequest(Method.POST, ApiResources.CreateAnonymousRegistrationResource);

            request.AddBody(registration);

            var response = ExecuteJsonRequest<accessTokenResponse>(request);
            StatusCode = response.StatusCode;
            Token = response.Data.access_token;
            return response.Data;
        }

        public accessTokenResponse EmailRegistration()
        {
            Console.WriteLine(@"In EmailRegistration");
            emailRegistrationRequest registration = new emailRegistrationRequest();
            registration.grant_type = "password";
            registration.username = Guid.NewGuid().ToString();
            registration.password = String.Concat(Guid.NewGuid().ToString(), "_password");
            registration.emailRegistration = new emailRegistration();
            registration.emailRegistration.dateOfBirth = "";
            registration.emailRegistration.emailAddress = String.Concat(Guid.NewGuid().ToString(), "@vmob.co");
            registration.emailRegistration.extendedData = "";
            registration.emailRegistration.firstName = "Smoke";
            registration.emailRegistration.lastName = "Test";
            registration.emailRegistration.homeCity = 0;
            registration.emailRegistration.gender = "m";
            registration.emailRegistration.fullName = "Smoke Test";

            var request = new JsonRestRequest(Method.POST, ApiResources.CreateEmailRegistrationResource);
            request.AddBody(registration);
            var response = ExecuteJsonRequest<accessTokenResponse>(request);
            StatusCode = response.StatusCode;
            Token = response.Data.access_token;
            return response.Data;
        }

        #endregion
    }
}