﻿using Newtonsoft.Json;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VMob.Automation.Api.Tool
{
    public class JsonDeserializer : IDeserializer
    {
        #region Fields

        private static string _errorMsg;

        #endregion

        #region Properties

        public string DateFormat
        { get; set; }

        public string Namespace
        { get; set; }

        public string RootElement
        { get; set; }

        #endregion

        #region Methods

        public static string GetError()
        {
            return _errorMsg;
        }

        public T Deserialize<T>(RestSharp.IRestResponse response)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception e)
            {
                var dictionary = response.Content
                                            .Split(',')
                                            .Select(part => part.Split(':'))
                                            .Where(part => part.Length == 2)
                                            .ToDictionary(sp => sp[0], sp => sp[1]);
                foreach (KeyValuePair<string, string> kvp in dictionary)
                {
                    _errorMsg = kvp.Value;
                }
                throw e;
            }
        }

        #endregion
    }
}