﻿using RestSharp;

namespace VMob.Automation.Api.Tool
{
    public class JsonRestRequest : RestRequest
    {
        #region Constructors

        public JsonRestRequest(Method method, string resource)
        {
            RequestFormat = DataFormat.Json;
            AddHeader("Content-Type", "application/json");
            JsonSerializer = new JsonSerializer();
            Method = method;
            Resource = resource;
        }

        #endregion
    }
}