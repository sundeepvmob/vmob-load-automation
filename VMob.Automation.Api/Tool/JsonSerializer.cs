﻿using Newtonsoft.Json;
using RestSharp.Serializers;
using System.IO;

namespace VMob.Automation.Api.Tool
{
    public class JsonSerializer : ISerializer
    {
        #region Fields

        private Newtonsoft.Json.JsonSerializer _jsonSerializer;

        #endregion

        #region Constructors

        public JsonSerializer()
        {
            _jsonSerializer = new Newtonsoft.Json.JsonSerializer();
            ContentType = "application/json";
        }

        #endregion

        #region Properties

        public string ContentType
        { get; set; }

        public string DateFormat
        { get; set; }

        public string Namespace
        { get; set; }

        public string RootElement
        { get; set; }

        #endregion

        #region Methods

        public string Serialize(object obj)
        {
            using (StringWriter sWriter = new StringWriter())
            {
                using (JsonTextWriter jsonWriter = new JsonTextWriter(sWriter))
                {
                    jsonWriter.Formatting = Formatting.Indented;
                    _jsonSerializer.Serialize(jsonWriter, obj);
                }
                return sWriter.ToString();
            }
        }

        #endregion
    }
}