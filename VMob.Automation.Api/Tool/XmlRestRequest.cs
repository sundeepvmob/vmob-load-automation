﻿using RestSharp;

namespace VMob.Automation.Api.Tool
{
    public class XmlRestRequest : RestRequest
    {
        #region Constructors

        public XmlRestRequest(Method method, string resource)
        {
            Method = method;
            Resource = resource;
            RequestFormat = DataFormat.Xml;
            AddHeader("Content-Type", "application/xml");
            //XmlSerializer = new XmlSerializer();
        }

        #endregion
    }
}