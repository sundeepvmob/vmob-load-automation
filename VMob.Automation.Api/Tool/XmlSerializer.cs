﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RestSharp.Serializers;

namespace APIAutomation.Tool
{
    public class XmlSerializer: ISerializer
    {
        public string ContentType { get; set; }

        public string DateFormat { get; set; }

        public string Namespace { get; set; }

        public string RootElement { get; set; }

        public string Serialize(object obj)
        {
            System.Xml.Serialization.XmlSerializer _xmlSerializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            using (StringWriter stringWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = new XmlTextWriter(stringWriter))
                {
                    _xmlSerializer.Serialize(xmlWriter,obj);
                    return stringWriter.ToString();
                }
            }
        }
    }
}
