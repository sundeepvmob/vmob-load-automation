﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VMob.Automation.Api.EndPoints;

namespace SmokeTest
{
    public class SmokeTestClass
    {
        #region Methods

        [Test]
        public void SmokeTest()
        {
            //Create anonymous registration
            RegistrationEndPoint registration = new RegistrationEndPoint();
            var token = registration.CreateAnonymousRegistration().access_token;
            Assert.IsNotNull(token, "Anonymous registration returned null");
            Assert.That(registration.StatusCode == HttpStatusCode.OK, "Anonymous registration: Expected 200 but returned " + registration.StatusCode);

            //Create email registration
            var accessToken = registration.EmailRegistration().access_token;
            Assert.IsNotNull(accessToken, "Email registration returned null");
            Assert.That(registration.StatusCode == HttpStatusCode.OK, "Email registration: Expected 200 but returned " + registration.StatusCode);

            //Get Configurations
            ConfigurationEndpoint configuration = new ConfigurationEndpoint();
            configuration.GetConfigurations(accessToken);
            Assert.That(configuration.StatusCode == HttpStatusCode.OK, "Get Configurations: Expected 200 but returned " + configuration.StatusCode);

            //Get Geofences
            OffersEndpoint offersObj = new OffersEndpoint();
            offersObj.GetGeofences(accessToken);
            Assert.That(configuration.StatusCode == HttpStatusCode.OK, "Get Geofences: Expected 200 but returned " + configuration.StatusCode);

            //Create Crossreference
            ConsumerEndpoint consumerObj = new ConsumerEndpoint();
            consumerObj.CreateCrossreference(accessToken);
            Assert.That(consumerObj.StatusCode == HttpStatusCode.Created, "Create Crossreference: Expected 201 but returned " + consumerObj.StatusCode);

            //Create RedeemedOffer
            consumerObj.CreateRedeemedOffer(accessToken);
            Assert.That(consumerObj.StatusCode == HttpStatusCode.OK, "Create RedeemedOffer: Expected 200 but returned " + consumerObj.StatusCode);

            //List Redeem Offers
            consumerObj.ListRedeemedOffers(accessToken);
            Assert.That(consumerObj.StatusCode == HttpStatusCode.OK, "List Redeem Offers: Expected 200 but returned " + consumerObj.StatusCode);

            //Search Offers
            offersObj.SearchOffers(accessToken);
            Assert.That(offersObj.StatusCode == HttpStatusCode.OK, "Search Offers: Expected 200 but returned " + offersObj.StatusCode);

            //Get Advertisements
            AdvertisementEndpoint adObject = new AdvertisementEndpoint();
            adObject.GetAdvertisements(accessToken);
            Assert.That(adObject.StatusCode == HttpStatusCode.OK, "Get Advertisements: Expected 200 but returned " + adObject.StatusCode);

            //List loyalty cards
            offersObj.ListLoyaltyCards(accessToken);
            Assert.That(offersObj.StatusCode == HttpStatusCode.OK, "List loyalty cards: Expected 200 but returned " + offersObj.StatusCode);

            //Weighted Content Search
            consumerObj.GetWeightedContent(accessToken);
            Assert.That(consumerObj.StatusCode == HttpStatusCode.OK, "Weighted Content Search: Expected 200 but returned " + consumerObj.StatusCode);

            //Create LocationCheckin
            ActivityEndpoint activityObj = new ActivityEndpoint();
            activityObj.CreateLocationCheckin(accessToken);
            Assert.That(activityObj.StatusCode == HttpStatusCode.Created, "Create LocationCheckin: Expected 201 but returned " + activityObj.StatusCode);

            //Create Offer Impression
            activityObj.CreateOfferImpression(accessToken);
            Assert.That(activityObj.StatusCode == HttpStatusCode.Created, "Create Offer Impression: Expected 201 but returned " + activityObj.StatusCode);

            //Create Offer Click
            activityObj.CreateOfferClick(accessToken);
            Assert.That(activityObj.StatusCode == HttpStatusCode.Created, "Create Offer Click: Expected 201 but returned " + activityObj.StatusCode);

            //Create favourite content
            consumerObj.CreateFavoriteContent(accessToken);
            Assert.That(consumerObj.StatusCode == HttpStatusCode.Created, "Create favourite content: Expected 201 but returned " + consumerObj.StatusCode);

            //Create Loyalty points
            var offerInstanceUniqueId = consumerObj.CreateLoyaltyCardPoints(accessToken).PointCreationSummary[0].OfferInstanceUniqueId;
            Assert.IsNotNull(offerInstanceUniqueId, "Create Loyalty points returned null");
            Assert.That(consumerObj.StatusCode == HttpStatusCode.Created, "Create Loyalty points: Expected 201 but returned " + consumerObj.StatusCode);

            //Create Redeemed Reward Offer
            consumerObj.RedeemedRewardOffer(accessToken, offerInstanceUniqueId);
            Assert.That(consumerObj.StatusCode == HttpStatusCode.OK, "Create Redeemed Reward Offer: Expected 200 but returned " + consumerObj.StatusCode);
        }

        #endregion
    }
}